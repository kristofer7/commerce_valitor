<?php

namespace Drupal\commerce_valitor;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Unicode;

/**
 * ValitorApi class implementation.
 */
class ValitorApi implements ValitorApiInterface {

  /**
   * Constant indicating error code for an undefined parameter.
   */
  const UNDEFINED_PARAM = 0;

  /**
   * Constant indicating error code for a missing parameter.
   */
  const MISSING_PARAM = 1;

  /**
   * Constant indicating error code for a bad parameter.
   */
  const BAD_PARAM = 2;

  /**
   * Constant indicating error code for a too long parameter.
   */
  const TOO_LONG_PARAM = 3;

  /**
   * Constant indicating error code for a too short parameter.
   */
  const TOO_SHORT_PARAM = 4;

  /**
   * Constant indicating error code for a undefined error.
   */
  const UNDEFINED_ERROR = 5;

  /**
   * Constant indicating error code for a timeout server response.
   */
  const SERVER_TIMEDOUT_ERROR = 6;

  /**
   * Constant indicating seconds for a timeout server response.
   */
  const SERVER_TIMEOUT = 60;


  /**
   * Constant indicating the contract number length.
   */
  const CONTRACT_NUMBER_LENGHT = 6;

  /**
   * Constant indicating the contract social security number length.
   */
  const CONTRACT_SS_NUMBER_LENGHT = 10;

  /**
   * Constant indicating the user password maxlength.
   */
  const PASSWORD_MAXLENGHT = 50;

  /**
   * Constant indicating the user name maxlength.
   */
  const USER_NAME_MAXLENGHT = 50;

  /**
   * Constant indicating the live environment.
   */
  const VALITOR_URL_LIVE = 'https://api.processing.valitor.com/Fyrirtaekjagreidslur/Fyrirtaekjagreidslur.asmx';

  /**
   * Constant indicating the test environment.
   */
  const VALITOR_URL_TEST = 'https://api.processing.uat.valitor.com/Fyrirtaekjagreidslur/Fyrirtaekjagreidslur.asmx';

  /**
   * Environment: live or test.
   */
  private $environment;

  /**
   * Merchant’s contract no. Numeric, 6 digits.
   */
  private $contractNumber;

  /**
   * Contract’s Icelandic SS no. Numeric, 10 digits.
   */
  private $contractSSNumber;

  /**
   * Merchant’s Web Service password. Alphanumeric, from 1 to 50 characters.
   */
  private $password;

  /**
   * POS terminal ID received from Valitor. Numeric, more than 1 digit.
   */
  private $posId;

  /**
   * Merchant’s Web Service user name. Alphanumeric, from 1 to 50 characters.
   */
  private $userName;

  /**
   * Constructs a new ValitorApi object.
   *
   * @param string $user_name
   *   Merchant’s Web Service user name. Alphanumeric, from 1 to 50 characters.
   * @param string $password
   *   Merchant’s Web Service password. Alphanumeric, from 1 to 50 characters.
   * @param integer $contract_number
   *   Merchant’s contract no. Numeric, 6 digits.
   * @param integer $contract_ss_number
   *  Contract’s Icelandic SS no. Numeric, 10 digits.
   * @param integer $pos_id
   *   POS terminal ID received from Valitor. Numeric, more than 1 digit.
   * @param string $environment
   *   Environment: live, test.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  public function __construct($user_name, $password, $contract_number, $contract_ss_number, $pos_id, $environment) {
    $this->setUserName($user_name)
      ->setPassword($password)
      ->setContractNumber($contract_number)
      ->setContractSSNumber($contract_ss_number)
      ->setPosId($pos_id)
      ->setEnvironment($environment);
  }

  /**
   * {@inheritdoc}
   */
  public static function getContractNumberLength() {
    return self::CONTRACT_NUMBER_LENGHT;
  }

  /**
   * {@inheritdoc}
   */
  public static function getCreditCardTypes() {
    return [
      'amex' => t('American Express'),
      'dinersclub' => t("Diners Club"),
      'discover' => t('Discover Card'),
      'jcb' => t('JCB'),
      'mastercard' => t('MasterCard'),
      'visa' => t('Visa'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function handleResponse($response = NULL) {
    if ((int) $response <= 0) {
      $msg = 'Transaction authorized';
    }
    else {
      switch ((int) $response) {
        case 1:
          $msg = 'Web Service ID no. does not exist.';
          break;

        case 2:
          $msg = 'Wrong user name or password.';
          break;

        case 3:
          $msg = 'The user name does not have accesss to the Web Service.';
          break;

        case 4:
          $msg = 'The user name does not have accesss to the Web Service from IP no.';
          break;

        case 200:
          $msg = 'Contract number is missing.';
          break;

        case 201:
          $msg = 'The contract number must be 6 digits. A 0 may be used as the first digit.';
          break;

        case 202:
          $msg = 'The contract number does not exist.';
          break;

        case 204:
          $msg = 'The contract number is not associated with the Icelandic SS number.';
          break;

        case 205:
          $msg = 'The contract owner’s Icelandic SS no. is missing.';
          break;

        case 206:
          $msg = 'The contract owner’s Icelandic SS no. must be 10 digits.';
          break;

        case 207:
          $msg = 'The Icelandic SS no. does not exist';
          break;

        case 208:
          $msg = 'The user name does not have access to the contract associated with the Icelandic SS no.';
          break;

        case 209:
          $msg = 'POS terminal ID is missing.';
          break;

        case 210:
          $msg = 'POS terminal ID must be in digits only.';
          break;

        case 211:
          $msg = 'Card no. is missing.';
          break;

        case 212:
          $msg = 'Card no. must be 11 to 19 digits.';
          break;

        case 213:
          $msg = 'Card no. is void.';
          break;

        case 214:
          $msg = 'Expiration date is missing.';
          break;

        case 215:
          $msg = 'Expiration date must be 4 digits.';

          break;

        case 216:
          $msg = 'Verification code is missing.';
          break;

        case 217:
          $msg = 'Verification code is too long.';
          break;

        case 218:
          $msg = 'Authorization not received for card no. – not possible to create a virtual card no.';
          break;

        case 219:
          $msg = 'Virtual card number is missing.';
          break;

        case 220:
          $msg = 'Virtual card number is void. Virtual card number is 16 digits where digits 2 through 9 are 99999.';
          break;

        case 221:
          $msg = 'Virtual card number does not exist.';
          break;

        case 222:
          $msg = 'The amount is missing.';
          break;

        case 223:
          $msg = 'The amount must be in digits.';
          break;

        case 224:
          $msg = 'The currency is missing.';
          break;

        case 225:
          $msg = 'The currency must be three letters.';
          break;

        case 226:
          $msg = 'The contract number is not listed for the currency.';
          break;

        case 227:
          $msg = 'The SS no. does not have access to the virtual card number.';
          break;

        case 228:
          $msg = 'Authorization not received.';
          break;

        case 229:
          $msg = 'Failed to reimburse.';
          break;

        case 230:
          $msg = 'The POS terminal ID is not associated with the SS no.';
          break;

        case 231:
          $msg = 'The card associated with virtual card number has expired.';
          break;

        case 232:
          $msg = 'Card expiration date is past.';
          break;

        case 233:
          $msg = 'The POS termainal ID is not associated with the contract number.';
          break;

        case 235:
          $msg = 'Error: location of card holder is too short.';
          break;

        case 236:
          $msg = 'Card expiration date is in wrong format.';
          break;

        case 246:
          $msg = 'Kortnúmer og kennitala korthafa passa hugsanlega ekki saman. Einnig gæti kortategund verið rangt valin. Ef debetkortið er með 16 stafa númer framan á sér að þá skal velja "Kreditkort eða 16 talna debetkort".';
          break;

        case 261:
          $msg = 'The debit card number is 10 digits long';
          break;

        case 500:
          $msg = 'System error.';
          break;

        case 999:
        default:
          $msg = 'Unknown error.';
          break;
      }
    }

    return $msg;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSSNumberLength() {
    return self::CONTRACT_SS_NUMBER_LENGHT;
  }

  /**
   * {@inheritdoc}
   */
  public static function getPasswordMaxLength() {
    return self::PASSWORD_MAXLENGHT;
  }

  /**
   * {@inheritdoc}
   */
  public static function getUserNameMaxLength() {
    return self::USER_NAME_MAXLENGHT;
  }

  /**
   * {@inheritdoc}
   */
  public static function validateContractNumber($contract_number) {
    if (Unicode::strlen($contract_number) > self::CONTRACT_NUMBER_LENGHT) {
      throw new PaymentGatewayException(t('The specified contract number: @contract_number is too long.', ['@contract_number' => $contract_number]), self::TOO_LONG_PARAM);
    }
    elseif (!ctype_digit($contract_number)) {
      throw new PaymentGatewayException(t('The contract number must be numeric.'), self::BAD_PARAM);
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function validatePassword($password) {
    if (Unicode::strlen($password) > self::PASSWORD_MAXLENGHT) {
      throw new PaymentGatewayException(t('The specified user name password is too long.'), self::TOO_LONG_PARAM);
    }
    elseif (Unicode::strlen($password) < 1) {
      throw new PaymentGatewayException(t('The specified user name password is too short.'), self::TOO_SHORT_PARAM);
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function validatePosId($pos_id) {
    if (Unicode::strlen($pos_id) < 1) {
      throw new PaymentGatewayException(t('The specified POS terminal ID is too short.'), self::TOO_SHORT_PARAM);
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function validateSecuritySocialNumber($ss_number) {
    if (Unicode::strlen($ss_number) > self::CONTRACT_SS_NUMBER_LENGHT) {
      throw new PaymentGatewayException(t('The specified contract social security number: @contract_ss_number is too long.', ['@contract_ss_number' => $ss_number]), self::TOO_LONG_PARAM);
    }
    elseif (Unicode::strlen($ss_number) < self::CONTRACT_SS_NUMBER_LENGHT) {
      throw new PaymentGatewayException(t('The specified contract social security number: @contract_ss_number is too short.', ['@contract_ss_number' => $ss_number]), self::TOO_SHORT_PARAM);
    }
    elseif (!ctype_digit($ss_number)) {
      throw new PaymentGatewayException(t('The contract social security number must be numeric.'), self::BAD_PARAM);
    }

    // @todo: Add SSN validation.

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function validateUserName($user_name) {
    if (Unicode::strlen($user_name) > self::USER_NAME_MAXLENGHT) {
      throw new PaymentGatewayException(t('The specified user name: @user_name is too long.', ['@user_name' => $user_name]), self::TOO_LONG_PARAM);
    }
    elseif (Unicode::strlen($user_name) < 1) {
      throw new PaymentGatewayException(t('The specified user name: @user_name is too short.', ['@user_name' => $user_name]), self::TOO_SHORT_PARAM);
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function cancelAuthorization($v_card_number, $currency, $transaction_id) {
    $params = [
      'Syndarkortnumer' => $v_card_number,
      'Gjaldmidill' => $currency,
      'Faerslunumer' => $transaction_id,
      'Stillingar' => '',
    ];

    return $this->sendRequest('FaOgildingu', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthorization($card_number, $card_exp_date, $card_cvv) {
    $params = [
      'Kortnumer' => $card_number,
      'Gildistimi' => $card_exp_date,
      'Oryggisnumer' => $card_cvv,
      'Stillingar' => '',
    ];

    return $this->sendRequest('FaSyndarkortnumer', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function seekAuthorization($v_card_number, $amount, $currency, $order_number) {
    if ($amount == 0) {
      throw new PaymentGatewayException(t('The charge amount must be larger than zero.'), self::BAD_PARAM);
    }
    $params = [
      'Syndarkortnumer' => $v_card_number,
      'Upphaed' => floatval($amount),
      'Gjaldmidill' => $currency,
      'Stillingar' => 'RD:' . $order_number,
    ];

    return $this->sendRequest('FaHeimild', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function getContractNumber() {
    return $this->contractNumber;
  }

  /**
   * {@inheritdoc}
   */
  public function setContractNumber($contract_number) {
    self::validateContractNumber($contract_number);

    return $this->set('contractNumber', $contract_number);
  }

  /**
   * {@inheritdoc}
   */
  public function getContractSSNumber() {
    return $this->contractSSNumber;
  }

  /**
   * {@inheritdoc}
   */
  public function setContractSSNumber($contract_ss_number) {
    self::validateSecuritySocialNumber($contract_ss_number);

    return $this->set('contractSSNumber', $contract_ss_number);
  }

  /**
   * {@inheritdoc}
   */
  public function getEnvironment() {
    return $this->environment;
  }

  /**
   * {@inheritdoc}
   */
  public function setEnvironment($environment) {
    if ($environment != 'live' && $environment != 'test') {
      throw new PaymentGatewayException(t('The specified environment: @environment is not valid.', ['@environment' => $environment]), self::BAD_PARAM);
    }

    return $this->set('environment', $environment);
  }

  /**
   * {@inheritdoc}
   */
  public function getEnvironmentURL() {
    $environment_url = self::VALITOR_URL_TEST;

    if ($this->getEnvironment() == 'live') {
      $environment_url = self::VALITOR_URL_LIVE;
    }

    return $environment_url;
  }

  /**
   * {@inheritdoc}
   */
  public function getPassword() {
    return $this->password;
  }

  /**
   * {@inheritdoc}
   */
  public function setPassword($password) {
    self::validatePassword($password);

    return $this->set('password', $password);
  }

  /**
   * {@inheritdoc}
   */
  public function getPosId() {
    return $this->posId;
  }

  /**
   * {@inheritdoc}
   */
  public function setPosId($pos_id) {
    self::validatePosId($pos_id);

    return $this->set('posId', $pos_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getUserName() {
    return $this->userName;
  }

  /**
   * {@inheritdoc}
   */
  public function setUserName($user_name) {
    self::validateUserName($user_name);

    return $this->set('userName', $user_name);
  }

  /**
   * {@inheritdoc}
   */
  public function updateExpirationDate($v_card_number, $exp_date) {
    $params = [
      'Syndarkortnumer' => $v_card_number,
      'NyrGildistimi' => $exp_date,
      'Stillingar' => '',
    ];

    return $this->sendRequest('UppfaeraGildistima', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function makeReimbursement($v_card_number, $amount, $currency) {
    $params = [
      'Syndarkortnumer' => $v_card_number,
      'Upphaed' => floatval($amount),
      'Gjaldmidill' => $currency,
      'Stillingar' => '',
    ];

    return $this->sendRequest('FaEndurgreitt', $params);
  }

  /**
   * Global setter.
   *
   * @param string $key
   *   Name of the property.
   * @param mixed $value
   *   Value to set.
   *
   * @return $this
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  protected function set($key, $value) {
    if (!property_exists($this, $key)) {
      throw new PaymentGatewayException(t('The property @property is not defined.', ['@property' => $key]), self::UNDEFINED_PARAM);
    }
    $this->{$key} = $value;

    return $this;
  }

  /**
   * Sends request to VALITOR.
   *
   * @param string $operation
   *   The operation to realize.
   * @param array $params
   *   An associative array with the parameters to send.

   * @return array
   *   An associative array, see the caller methods.
   *
   * @see ValitorApi::getAuthorization()
   * @see ValitorApi::seekAuthorization()
   * @see ValitorApi::makeReimbursement()
   * @see ValitorApi::cancelAuthorization()
   * @see ValitorApi::getLastFourDigits()
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  protected function sendRequest($operation, $params) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $this->getEnvironmentURL() . '/' . $operation);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, self::SERVER_TIMEOUT);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(
      [
        'Notandanafn' => $this->getUserName(),
        'Lykilord' => $this->getPassword(),
        'Samningsnumer' => $this->getContractNumber(),
        'SamningsKennitala' => $this->getContractSSNumber(),
        'PosiID' => $this->getPosId(),
      ] + $params
    ));
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded; charset=UTF-8']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    if ($this->getEnvironment() == 'test') {
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    }
    $response = curl_exec($ch);

    // Check if any error occurred
    if (curl_errno($ch) == CURLE_OPERATION_TIMEDOUT) {
      curl_close($ch);
      throw new PaymentGatewayException(t('Request timeout: The request to valitor server has timed out, please, reply.'), self::SERVER_TIMEDOUT_ERROR);
    }
    elseif (curl_errno($ch)) {
      curl_close($ch);
      throw new PaymentGatewayException(t('Request error: An error has occurred trying to get the response.'), self::UNDEFINED_ERROR);
    }
    else {
      curl_close($ch);

      libxml_use_internal_errors(true);
      try {
        $result = Json::decode(
          Json::encode(
            new \SimpleXMLElement($response)
          )
        );
      } catch (\Exception $e){
        /** @var \libXMLError $error */
        foreach (libxml_get_errors() as $error) {
          \Drupal::logger('commerce_valitor')->error($error->message);
        }
        libxml_clear_errors();
        drupal_set_message(t('Undefined error occurred and the process was terminated with the following message: @message. Please contact administrator or try again later' , ['@message' => $e->getMessage()]), 'error');
        $result['Villunumer'] = 999;
      }
      return $result;
    }
  }

}
