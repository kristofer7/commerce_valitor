<?php

namespace Drupal\commerce_valitor;

/**
 * Provides the interface for the ValitorApi library.
 */
interface ValitorApiInterface {

  /**
   * Gets contract number length.
   *
   * @return integer
   *   Return contract number length.
   */
  public static function getContractNumberLength();

  /**
   * Get supported credit card types.
   *
   * @return array
   *   Return an array with all supported debit card types.
   */
  public static function getCreditCardTypes();

  /**
   * Handle the response of the payment transaction.
   *
   * Messages from "Corporate Payments - Specification API" v1.0.16 - 02.06.2017.
   *
   * @param integer $response
   *   The response feedback code.
   *
   * @return string
   *   The handle response message.
   */
  public static function handleResponse($response = NULL);

  /**
   * Gets contract social security number length.
   *
   * @return integer
   *   Return contract social security number length.
   */
  public static function getSSNumberLength();

  /**
   * Gets user password maxlength.
   *
   * @return integer
   *   Return user password maxlength.
   */
  public static function getPasswordMaxLength();

  /**
   * Gets user name maxlength.
   *
   * @return integer
   *   Return user name maxlength.
   */
  public static function getUserNameMaxLength();

  /**
   * @param integer $contract_number
   *   The contract number.
   *
   * @return boolean
   *   TRUE if the contract number is valid.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  public static function validateContractNumber($contract_number);

  /**
   * @param string $password
   *   The user name password.
   *
   * @return boolean
   *   TRUE if the user name password is valid.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  public static function validatePassword($password);

  /**
   * @param integer $pos_id
   *   The POS terminal ID.
   *
   * @return boolean
   *   TRUE if the POS terminal ID is valid.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  public static function validatePosId($pos_id);

  /**
   * @param integer $ss_number
   *   The security social number.
   *
   * @return boolean
   *   TRUE if the security social number is valid.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  public static function validateSecuritySocialNumber($ss_number);

  /**
   * @param string $user_name
   *   The user name.
   *
   * @return boolean
   *   TRUE if the user name is valid.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  public static function validateUserName($user_name);

  /**
   * Cancel authorization, that has not been settled.
   *
   * @param integer $v_card_number
   *   Virtual card no. received from the function FaSyndarkortnumer.
   * @param string $currency
   *   Currency, e.g. ISK, USD, EUR, DKK.
   * @param integer $transaction_id
   *   TransactionId on the transaction that is being.
   *
   * @return array
   *   An associative array keyered by its field names.
   *   - Villuskilabod: Error description. Is empty if there is no error.
   *   - Villunumer: Error no. Is 0 if no error.
   *   - VilluLogID: Error ID in Valitor logs.
   *   - Kvittun: Receipts.
   *     - VerslunNafn: Name of store.
   *     - VerslunHeimilisfang: Store address.
   *     - VerslunStadur: Place of store.
   *     - TegundKorts: Card type, e.g. VISA.
   *     - TegundKortsKodi: Card type code. Example:
   *         400 (domestic VISA)
   *         401 (foreign VISA)
   *         500 (MasterCard)
   *         510 (JCB)
   *         520 (Diners)
   *         530 (AMEX)
   *         540 (Discover)
   *     - Dagsetning: Authorization date.
   *     - Timi: Authorization time.
   *     - Kortnumer: Starred card no. where last 4 digits appear.
   *     - Upphaed: Amount.
   *     - Faerslunummer: Transaction no.
   *     - Faersluhirdir: Acquirer name and phone no.
   *     - Heimildarnumer: Authorization no.
   *     - StadsetningNumer: Location no., default 0001.
   *     - UtstodNumer: Terminal no., default 0001.
   *     - BuidAdOgilda: True if transaction canceled.
   *     - Bunkanumer: Batch numbers with prefix B.
   *     - Soluadilinumer: Merchant contract no. with prefix S.
   *     - Hugbunadarnumer: Permanent no.: 11101020000.
   *     - PosiID: POS terminal ID allocated by Valitor.
   *     - PinSkilabod: Not used.
   *     - Vidskiptaskilabod: Text, currency and amount.
   *     - F22_1til4: Permanent no.: 5101.
   *     - LinaC1: Permanent no.: 060.
   *     - LinaC2: Permanent no.: 1509.
   *     - LinaC3: Text and transaction no.
   *     - LinaC4: Text and authorization no.
   *     - LinaD1: Not used.
   *     - LinaD2: Not used.
   *     - TegundAdgerd: Permanent text: SÍMGREIÐSLA.
   *     - FaerslunumerUpphafleguFaerslu: Not used.
   *     - TerminalID: Terminal ID.
   */
  public function cancelAuthorization($v_card_number, $currency, $transaction_id);

  /**
   * Get the virtual credit card number.
   *
   * @param integer $card_number
   *   Card no. used to create a virtual card no. for.
   * @param integer $card_exp_date
   *   Card expiration date written MMYY.
   * @param integer $card_cvv
   *   Verification code on reverse side of card.
   *
   * @return array
   *   An associative array keyered by its field names.
   *   - Villuskilabod: Error description. Is empty if there is no error.
   *   - Villunumer: Error no. Is 0 if no error.
   *   - VilluLogID: Error ID in Valitor logs.
   *   - Syndarkortnumer: Virtual card no.
   */
  public function getAuthorization($card_number, $card_exp_date, $card_cvv);

  /**
   * Seek authorization, that is, when the card is to be charged that is behind
   * the virtual card number.
   *
   * @param string $v_card_number
   *   Virtual card no. received from the function FaSyndarkortnumer.
   * @param integer $amount
   *   Amount to be charged.
   * @param string $currency
   *   Currency, e.g. ISK, USD, EUR, DKK.
   * @param string $order_number
   *   Order Number.
   *
   * @return array
   *   An associative array keyered by its field names.
   *   - Villuskilabod: Error description. Is empty if there is no error.
   *   - Villunumer: Error no. Is 0 if no error.
   *   - VilluLogID: Error ID in Valitor logs.
   *   - Kvittun: Receipts.
   *     - VerslunNafn: Name of store.
   *     - VerslunHeimilisfang: Store address.
   *     - VerslunStadur: Place of store.
   *     - TegundKorts: Card type, e.g. VISA.
   *     - TegundKortsKodi: Card type code. Example:
   *         400 (domestic VISA)
   *         401 (foreign VISA)
   *         500 (MasterCard)
   *         510 (JCB)
   *         520 (Diners)
   *         530 (AMEX)
   *         540 (Discover)
   *     - Dagsetning: Authorization date.
   *     - Timi: Authorization time.
   *     - Kortnumer: Starred card no. where last 4 digits appear.
   *     - Upphaed: Amount.
   *     - Faerslunummer: Transaction no.
   *     - Faersluhirdir: Acquirer name and phone no.
   *     - Heimildarnumer: Authorization no.
   *     - StadsetningNumer: Location no., default 0001.
   *     - UtstodNumer: Terminal no., default 0001.
   *     - BuidAdOgilda: True if transaction canceled.
   *     - Bunkanumer: Batch numbers with prefix B.
   *     - Soluadilinumer: Merchant contract no. with prefix S.
   *     - Hugbunadarnumer: Permanent no.: 11101020000.
   *     - PosiID: POS terminal ID allocated by Valitor.
   *     - PinSkilabod: Not used.
   *     - Vidskiptaskilabod: Text, currency and amount.
   *     - F22_1til4: Permanent no.: 5101.
   *     - LinaC1: Permanent no.: 060.
   *     - LinaC2: Permanent no.: 1509.
   *     - LinaC3: Text and transaction no.
   *     - LinaC4: Text and authorization no.
   *     - LinaD1: Not used.
   *     - LinaD2: Not used.
   *     - TegundAdgerd: Permanent text: SÍMGREIÐSLA.
   *     - FaerslunumerUpphafleguFaerslu: Not used.
   *     - TerminalID: Terminal ID.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *
   * @see getAuthorization()
   */
  public function seekAuthorization($v_card_number, $amount, $currency, $order_number);

  /**
   * Getter for ValitorApi::$contractNumber variable.
   *
   * @return integer
   *   Return the requested variable.
   */
  public function getContractNumber();

  /**
   * Setter for ValitorApi::$contractNumber variable.
   *
   * @param integer $contract_number
   *   The variable to set.
   *
   * @return $this
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  public function setContractNumber($contract_number);

  /**
   * Getter for ValitorApi::$contractSSNumber variable.
   *
   * @return integer
   *   Return the requested variable.
   */
  public function getContractSSNumber();

  /**
   * Setter for ValitorApi::$contractSSNumber variable.
   *
   * @param integer $contract_ss_number
   *   The variable to set.
   *
   * @return $this
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  public function setContractSSNumber($contract_ss_number);

  /**
   * Getter for ValitorApi::$environment variable.
   *
   * @return string
   *   Return the requested variable.
   */
  public function getEnvironment();

  /**
   * Setter for ValitorApi::$environment property.
   *
   * @param string $environment
   *   The property to set.
   *
   * @return $this
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  public function setEnvironment($environment);

  /**
   * Getter for environment URL.
   *
   * @return string
   *   Return the requested environment URL.
   */
  public function getEnvironmentURL();

  /**
   * Getter for ValitorApi::$password variable.
   *
   * @return string
   *   Return the requested variable.
   */
  public function getPassword();

  /**
   * Setter for ValitorApi::$password variable.
   *
   * @param string $password
   *   The variable to set.
   *
   * @return $this
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  public function setPassword($password);

  /**
   * Getter for ValitorApi::$posId variable.
   *
   * @return string
   *   Return the requested variable.
   */
  public function getPosId();

  /**
   * Setter for ValitorApi::$posId variable.
   *
   * @param integer $pos_id
   *   The variable to set.
   *
   * @return $this
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  public function setPosId($pos_id);

  /**
   * Getter for ValitorApi::$userName variable.
   *
   * @return string
   *   Return the requested variable.
   */
  public function getUserName();

  /**
   * Setter for ValitorApi::$userName variable.
   *
   * @param string $user_name
   *   The variable to set.
   *
   * @return $this
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  public function setUserName($user_name);

  /**
   * Update the expiration date (valid period) of a card associated with a
   * virtual card number.
   *
   * @param integer $v_card_number
   *   Virtual card number received from the function FaSyndarkortnumer.
   * @param integer $exp_date
   *   New expiration date.
   *
   * @return array
   *   An associative array keyered by its field names.
   *   - Villuskilabod: Error description. Is empty if there is no error.
   *   - Villunumer: Error no. Is 0 if no error.
   *   - VilluLogID: Error ID in Valitor logs.
   */
  public function updateExpirationDate($v_card_number, $exp_date);

  /**
   * Reimbursement, that is, when a sum is to be deposited on a card associated
   * with a virtual card number.
   *
   * @param string $v_card_number
   *   Virtual card no. received from the function FaSyndarkortnumer.
   * @param integer $amount
   *   Amount to be charged.
   * @param string $currency
   *   Currency, e.g. ISK, USD, EUR, DKK.
   *
   * @return array
   *   An associative array keyered by its field names.
   *   - Villuskilabod: Error description. Is empty if there is no error.
   *   - Villunumer: Error no. Is 0 if no error.
   *   - VilluLogID: Error ID in Valitor logs.
   *   - Kvittun: Receipts.
   *     - VerslunNafn: Name of store.
   *     - VerslunHeimilisfang: Store address.
   *     - VerslunStadur: Place of store.
   *     - TegundKorts: Card type, e.g. VISA.
   *     - TegundKortsKodi: Card type code. Example:
   *         400 (domestic VISA)
   *         401 (foreign VISA)
   *         500 (MasterCard)
   *         510 (JCB)
   *         520 (Diners)
   *         530 (AMEX)
   *         540 (Discover)
   *     - Dagsetning: Authorization date.
   *     - Timi: Authorization time.
   *     - Kortnumer: Starred card no. where last 4 digits appear.
   *     - Upphaed: Amount.
   *     - Faerslunummer: Transaction no.
   *     - Faersluhirdir: Acquirer name and phone no.
   *     - Heimildarnumer: Authorization no.
   *     - StadsetningNumer: Location no., default 0001.
   *     - UtstodNumer: Terminal no., default 0001.
   *     - BuidAdOgilda: True if transaction canceled.
   *     - Bunkanumer: Batch numbers with prefix B.
   *     - Soluadilinumer: Merchant contract no. with prefix S.
   *     - Hugbunadarnumer: Permanent no.: 11101020000.
   *     - PosiID: POS terminal ID allocated by Valitor.
   *     - PinSkilabod: Not used.
   *     - Vidskiptaskilabod: Text, currency and amount.
   *     - F22_1til4: Permanent no.: 5101.
   *     - LinaC1: Permanent no.: 060.
   *     - LinaC2: Permanent no.: 1509.
   *     - LinaC3: Text and transaction no.
   *     - LinaC4: Text and authorization no.
   *     - LinaD1: Not used.
   *     - LinaD2: Not used.
   *     - TegundAdgerd: Permanent text: SÍMGREIÐSLA.
   *     - FaerslunumerUpphafleguFaerslu: Not used.
   *     - TerminalID: Terminal ID.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *
   * @see getAuthorization()
   */
  public function makeReimbursement($v_card_number, $amount, $currency);

}
