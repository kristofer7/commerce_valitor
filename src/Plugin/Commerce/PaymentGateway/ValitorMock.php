<?php

namespace Drupal\commerce_valitor\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\commerce_valitor\ValitorApi;
use Drupal\commerce_valitor\ValitorApiInterface;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the VALITOR payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "commerce_valitor_mock",
 *   label = "VALITOR MOCK (On-site)",
 *   display_label = "Pay with credit card - valitor mock",
 *   forms = {
 *     "edit-payment-method" = "Drupal\commerce_valitor\PluginForm\PaymentMethodEditForm",
 *     "refund-payment" = "Drupal\commerce_valitor\PluginForm\PaymentRefundForm"
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex",
 *     "dinersclub",
 *     "discover",
 *     "jcb",
 *     "mastercard",
 *     "visa",
 *   },
 * )
 */
class ValitorMock extends Valitor {
  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    // Authorization is made when ::createPaymentMethod.
    if (!$capture) {
      return;
    }
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $valitor_api = $this->initializeValitorApi();
    $response = [];

    $response['Villunumer'] = 0;
    $response['Kvittun']['Faerslunumer'] = 'MockedGateway';
    $payment_method->setRemoteId('12345');

    if ($response['Villunumer'] > 0) {
      throw new PaymentGatewayException($this->t($valitor_api->handleResponse($response['Villunumer'])));
    }

    $payment->setState('completed');
    $payment->setRemoteId($response['Kvittun']['Faerslunumer'] . '|' . $payment_method->getRemoteId());
    $payment->setAuthorizedTime(\Drupal::time()->getRequestTime());
    $payment->save();
  }

}
