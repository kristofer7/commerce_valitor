<?php

namespace Drupal\commerce_valitor\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Exception\SoftDeclineException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\commerce_valitor\ValitorApi;
use Drupal\commerce_valitor\ValitorApiInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the VALITOR payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "commerce_valitor",
 *   label = @Translation("VALITOR (On-site)"),
 *   display_label = @Translation("Pay with credit card"),
 *   forms = {
 *     "edit-payment-method" = "Drupal\commerce_valitor\PluginForm\PaymentMethodEditForm",
 *     "refund-payment" = "Drupal\commerce_valitor\PluginForm\PaymentRefundForm"
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex",
 *     "dinersclub",
 *     "discover",
 *     "jcb",
 *     "mastercard",
 *     "visa",
 *   },
 * )
 */
class Valitor extends OnsitePaymentGatewayBase implements ValitorInterface {

  /**
   * @var LoggerInterface
   */
  protected $logger;

  /** @var MessengerInterface */
  protected $messenger;

  /**
   * Valitor constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param EntityTypeManagerInterface $entity_type_manager
   * @param PaymentTypeManager $payment_type_manager
   * @param PaymentMethodTypeManager $payment_method_type_manager
   * @param TimeInterface $time
   * @param LoggerInterface $logger
   * @param MessengerInterface $messenger
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, LoggerInterface $logger, MessengerInterface $messenger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->logger = $logger;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.channel.commerce_payment'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'user_name' => '',
      'password' => '',
      'contract_number' => '',
      'contract_ss_number' => '',
      'pos_id' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['user_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User name'),
      '#description' => $this->t("Merchant’s web service user name."),
      '#default_value' => $this->configuration['user_name'],
      '#maxlength' => ValitorApi::getUserNameMaxLength(),
      '#element_validate' => [[get_called_class(), 'validateUserName']],
      '#required' => TRUE,
    ];
    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#description' => $this->t("Merchant’s web service user name password."),
      '#default_value' => $this->configuration['password'],
      '#maxlength' => ValitorApi::getPasswordMaxLength(),
      '#element_validate' => [[get_called_class(), 'validatePassword']],
      '#required' => TRUE,
    ];
    $form['contract_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Contract number'),
      '#description' => $this->t("Merchant’s contract number."),
      '#default_value' => $this->configuration['contract_number'],
      '#maxlength' => ValitorApi::getContractNumberLength(),
      '#element_validate' => [[get_called_class(), 'validateContractNumber']],
      '#required' => TRUE,
    ];
    $form['contract_ss_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Contract social security number'),
      '#description' => $this->t("Contract’s Icelandic social security number."),
      '#default_value' => $this->configuration['contract_ss_number'],
      '#maxlength' => ValitorApi::getSSNumberLength(),
      '#element_validate' => [[get_called_class(), 'validateContractSsNumber']],
      '#required' => TRUE,
    ];
    $form['pos_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('POS ID'),
      '#description' => $this->t('POS terminal ID received from Valitor.'),
      '#default_value' => $this->configuration['pos_id'],
      '#element_validate' => [[get_called_class(), 'validatePosId']],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * Form element validation handler for the 'contract_number' element.
   *
   * @see Valitor::buildConfigurationForm()
   */
  public static function validateContractNumber($element, FormStateInterface $form_state, $form) {
    $value = $element['#value'];

    try {
      ValitorApi::validateContractNumber($value);
    }
    catch (PaymentGatewayException $e) {
      $form_state->setError($element, t('The contract number is not valid.'));
    }

    return;
  }

  /**
   * Form element validation handler for the 'contract_ss_number' element.
   *
   * @see Valitor::buildConfigurationForm()
   */
  public static function validateContractSsNumber($element, FormStateInterface $form_state, $form) {
    $value = $element['#value'];

    try {
      ValitorApi::validateSecuritySocialNumber($value);
    }
    catch (PaymentGatewayException $e) {
      $form_state->setError($element, t('The security social number is not valid.'));
    }

    return;
  }

  /**
   * Form element validation handler for the 'password' element.
   *
   * @see Valitor::buildConfigurationForm()
   */
  public static function validatePassword($element, FormStateInterface $form_state, $form) {
    $value = $element['#value'];

    try {
      ValitorApi::validatePassword($value);
    }
    catch (PaymentGatewayException $e) {
      $form_state->setError($element, t('The contract user name password is not valid.'));
    }

    return;
  }

  /**
   * Form element validation handler for the 'password' element.
   *
   * @see Valitor::buildConfigurationForm()
   */
  public static function validatePosId($element, FormStateInterface $form_state, $form) {
    $value = $element['#value'];

    try {
      ValitorApi::validatePosId($value);
    }
    catch (PaymentGatewayException $e) {
      $form_state->setError($element, t('The POS terminal ID is not valid.'));
    }

    return;
  }

  /**
   * Form element validation handler for the 'user_name' element.
   *
   * @see Valitor::buildConfigurationForm()
   */
  public static function validateUserName($element, FormStateInterface $form_state, $form) {
    $value = $element['#value'];

    try {
      ValitorApi::validateUserName($value);
    }
    catch (PaymentGatewayException $e) {
      $form_state->setError($element, t('The contract user name is not valid.'));
    }

    return;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['user_name'] = $values['user_name'];
      $this->configuration['password'] = $values['password'];
      $this->configuration['contract_number'] = $values['contract_number'];
      $this->configuration['contract_ss_number'] = $values['contract_ss_number'];
      $this->configuration['pos_id'] = $values['pos_id'];
    }
  }

  /**
   * Throws the proper exception according to the response code.
   *
   * Valitor sends in response the status code of the error.
   * @see \Drupal\commerce_valitor\ValitorApi for the reference.
   *
   * @param string $message
   *   The description of the error.
   * @param int $status_code
   *   The numeric code of the error.
   */
  private function throwException($message, $status_code) {
    // Status codes that allow to try to charge again.
    // The problem was with Valitor system, or just some
    // unexpected exception.
    $soft_decline_codes = [
      1, 2, 3, 4, 200, 201, 202, 204, 205, 206, 207, 208, 209, 210,
      222, 223, 224, 225, 226, 227, 228, 230, 235, 500, 999
    ];
    // Status code that do not allow to charge again.
    // The payment will not be successful with the given card.
    $hard_decline_codes = [
      219, 220, 221, 231
    ];
    $user_messages = [
      211 => 'Credit card number missing',
      212 => 'Creditcard number must be 11 to 19 numeric characters',
      213 => 'Creditcard number is invalid',
      214 => 'Credit card expiration date missing',
      215 => 'Expiration date must be 4 numeric characters',
      216 => 'Security number missing',
      217 => 'Security number is too long',
      218 => 'Not authorised',
      232 => 'Credit card expired',
    ];
    // Throw the correct exception, so the charging can go into dunning
    // or the payment changes to failure status.
    if (in_array($status_code, $soft_decline_codes)) {
      $this->logger->warning($message);
      $user_message = $this->t('An error occurred while performing payment, please contact us either by phone or email');
      $this->messenger->addMessage($user_message, MessengerInterface::TYPE_ERROR);
      throw new SoftDeclineException($message, $status_code);
    } elseif (in_array($status_code, $hard_decline_codes)) {
      $this->logger->error($message);
      $user_message = $this->t('Virtual card number missing, please try deleting the credit card and try registering again');
      $this->messenger->addMessage($user_message, MessengerInterface::TYPE_ERROR);
      throw new HardDeclineException($message, $status_code);
    } else {
      if (!empty($user_messages[$status_code])) {
        $user_message = $this->t($user_messages[$status_code]);
        $this->messenger->addMessage($user_message, MessengerInterface::TYPE_ERROR);
      }
      // The PaymentGatewayException is logged in commerce module.
      throw new PaymentGatewayException($message, $status_code);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    // Authorization is made when ::createPaymentMethod.
    if (!$capture) {
      return;
    }
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $valitor_api = $this->initializeValitorApi();
    $response = $valitor_api->seekAuthorization($payment_method->getRemoteId(), $payment->getAmount()->getNumber(), $payment->getAmount()->getCurrencyCode(), $payment->getOrderId());

    // Process response errors.
    if ($response['Villunumer'] > 0) {
      $message = $this->t($valitor_api->handleResponse($response['Villunumer']));
      $this->throwException($message, $response['Villunumer']);
    }

    $payment->setState('completed');
    $payment->setRemoteId($response['Kvittun']['Faerslunumer'] . '|' . $payment_method->getRemoteId());
    $payment->setAuthorizedTime($this->time->getRequestTime());
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    $valitor_api = $this->initializeValitorApi();
    $response = $valitor_api->seekAuthorization($payment_method->getRemoteId(), $amount->getNumber(), $amount->getCurrencyCode(), $payment->getOrderId());

    // Process response errors.
    if ($response['Villunumer'] > 0) {
      $message = $this->t($valitor_api->handleResponse($response['Villunumer']));
      $this->throwException($message, $response['Villunumer']);
    }

    $payment->setState('completed');
    $payment->setRemoteId($response['Kvittun']['Faerslunumer'] . '|' . $payment_method->getRemoteId());
    $payment->setAmount($amount);
    $payment->setAuthorizedTime($this->time->getRequestTime());
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $valitor_api = $this->initializeValitorApi();
    $response = $valitor_api->cancelAuthorization($payment_method->getRemoteId(), $payment->getAmount()->getCurrencyCode(), reset(explode('|', $payment->getRemoteId())));

    // Check if there is any error number. Is 0 if no error.
    if ($response['Villunumer'] > 0) {
      $message = $this->t($valitor_api->handleResponse($response['Villunumer']));
      $this->throwException($message, $response['Villunumer']);
    }

    $payment->setState('authorization_voided');
    $payment->setRemoteId($response['Kvittun']['Faerslunumer'] . '|' . $payment_method->getRemoteId());
    $payment->setAuthorizedTime($this->time->getRequestTime());
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    $valitor_api = $this->initializeValitorApi();
    $response = $valitor_api->makeReimbursement($payment_method->getRemoteId(), $amount->getNumber(), $amount->getCurrencyCode());

    // Check if there is any error number. Is 0 if no error.
    if ($response['Villunumer'] > 0) {
      $message = $this->t($valitor_api->handleResponse($response['Villunumer']));
      $this->throwException($message, $response['Villunumer']);
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->setRemoteId($response['Kvittun']['Faerslunumer'] . '|' . $payment_method->getRemoteId());
    $payment->setAuthorizedTime($this->time->getRequestTime());
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    foreach (['type', 'number', 'expiration'] as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new \InvalidArgumentException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    $payment_method->card_type = $payment_details['type'];
    $payment_method->card_number = substr($payment_details['number'], -4);
    $payment_method->card_exp_month = $payment_details['expiration']['month'];
    $payment_method->card_exp_year = $payment_details['expiration']['year'];
    $expires = CreditCard::calculateExpirationTimestamp($payment_details['expiration']['month'], $payment_details['expiration']['year']);

    // Get the virtual credit card number.
    $valitor_api = $this->initializeValitorApi();
    $response = $valitor_api->getAuthorization($payment_details['number'], $payment_details['expiration']['month'] . Unicode::substr($payment_details['expiration']['year'], -2), $payment_details['security_code']);

    // Check if there is any error number. Is 0 if no error.
    if ($response['Villunumer'] > 0) {
      $message = $this->t($valitor_api->handleResponse($response['Villunumer']));
      $this->throwException($message, $response['Villunumer']);
    }

    $payment_method->setRemoteId($response['Syndarkortnumer']);
    $payment_method->setExpiresTime($expires);
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentMethod(PaymentMethodInterface $payment_method) {
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\CreditCard $payment_method */
    $valitor_api = $this->initializeValitorApi();
    $response = $valitor_api->updateExpirationDate($payment_method->getRemoteId(), $payment_method->card_exp_month->value . Unicode::substr($payment_method->card_exp_year->value, -2));

    if ($response['Villunumer'] > 0) {
      $message = $this->t($valitor_api->handleResponse($response['Villunumer']));
      $this->throwException($message, $response['Villunumer']);
    }

    $payment_method->save();
  }

  /**
   * Initialize a new ValitorApi object.
   *
   * @return \Drupal\commerce_valitor\ValitorApiInterface
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  protected function initializeValitorApi() {
    static $valitor_api = NULL;

    if (!$valitor_api instanceof ValitorApiInterface) {
      $valitor_api = new ValitorApi(
        $this->configuration['user_name'],
        $this->configuration['password'],
        $this->configuration['contract_number'],
        $this->configuration['contract_ss_number'],
        $this->configuration['pos_id'],
        $this->configuration['mode']
      );
    }

    return $valitor_api;
  }
}
